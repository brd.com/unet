function isObject(o) {
  return typeof o == 'object';
}

function isObjectObject(o) {
  return isObject(o) === true
    && Object.prototype.toString.call(o) === '[object Object]';
}

// from is-plain-object:
function isPlain(o) {
  var ctor = o.constructor;

  // If has modified constructor
  if (typeof ctor !== 'function') return false;
  
  // If has modified prototype
  var prot = ctor.prototype;
  if (isObjectObject(prot) === false) return false;
  
  // If constructor does not have an Object-specific method
  if (prot.hasOwnProperty('isPrototypeOf') === false) {
    return false;
  }
  
  // Most likely a plain Object
  return true;
}

async function jsonMiddleware(options={},next) {
  if(options.skipJSON === true) {
    return await next(options);
  }
  
  var headers = Object.assign({},options.headers);
  var body = options.body;

  if(typeof options.body == 'object' && (Array.isArray(options.body) || isPlain(options.body))) {
    body = JSON.stringify(options.body),
    headers['content-type'] = 'application/json'
  }

  headers['accept'] = headers['accept'] || 'application/json';

  var response = await next(Object.assign({},options,{ headers: headers, body: body }));

  var type = response && response.headers && response.headers['content-type'];

  if(type && type.match(/^(application|text)\/json/)) {
    var json = await response.json()

    if(Math.floor(response.status/100) != 2) {
      return { error: json, response: response }
    }

    return { result: json, response: response };
  }

  if(Math.floor(response.status/100) != 2) {
    return { error: response.error && response.error.message || `${response.status} - ${response.statusText}`, response }
  }

  return { response };
}

function defaultMethodMiddleware(options,next) {
  let method = options.method || (options.body ? "POST" : "GET");

  return next(Object.assign({ method: method },options));
}

var DEFAULT_MIDDLEWARE = [
  jsonMiddleware,
  defaultMethodMiddleware,
];

module.exports = function(execute) {
  return function net(options={},extend=false) {
    if(extend==true) {
      return (newOptions={}) => net(Object.assign({},options,newOptions));
    }

    if(typeof options == 'string') {
      options = { url: options, method: 'GET' };
    }

    var middleware = 'middleware' in options ? options.middleware : DEFAULT_MIDDLEWARE;

    function caller(middleware) {
      return function(options) {
        if(!options) throw new Error("Must pass options to next()");
        var layer = middleware[0];
        var fn = layer.handler || layer;
        if(typeof fn != 'function') return;
        var next = caller(middleware.slice(1));

        return fn(options,next);
      };
    }

    var ret = caller(middleware.concat(execute))(options);

    return ret;
  }
}

module.exports.DEFAULT_MIDDLEWARE = DEFAULT_MIDDLEWARE;
module.exports.middleware = {
  default: DEFAULT_MIDDLEWARE,
  json: jsonMiddleware,
  defaultMethod: defaultMethodMiddleware,
};
