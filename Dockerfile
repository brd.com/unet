FROM node:10

# Make directories to store the dependencies and the application code:

RUN mkdir -p /application
WORKDIR /application

# Set up the basics:

ENV PORT 8080
EXPOSE 8080
ENV DOCKER_ENV docker
ENV PATH /application/node_modules/.bin:$PATH
ENV NODE_ENV development

# Make incremental updates to the dependencies:

COPY package*.json /application/
RUN npm install

RUN shasum package.json > /package.json-shasum

# By default, run the application with node:

CMD [ "bin/dev" ]
