const unet = require('./shared');
const URL = require('url').URL;

const PROTOCOLS = {
  'https:': require('https'),
  'http:': require('http'),
};

function execute(options, next) {
  return new Promise((resolve, reject) => {
    try {
      let method = options.method;
      let url = new URL(options.url||options.uri,options.baseURL || (process.env.PORT && `http://localhost:${process.env.PORT}`) || undefined);

      if(!method) throw new Error("Must specify method");
      if(!url) throw new Error("must specify url");
      
      let protocol = url.protocol;
      let proto = PROTOCOLS[url.protocol];

      let headers = {};

      for(var k in options.headers) {
        var c = options.headers[k];
        if(typeof c != 'undefined') {
          headers[k] = c;
        }
      }
      
      const body = options.body;
      
      if(body && (typeof body == 'string' || Buffer.isBuffer(body) || ('length' in body)) && options.automaticContentLength !== false) {
        headers['content-length'] = headers['content-length'] || body.length
      }

      let opts = {
        method,
        hostname: url.hostname,
        port: url.port || {'https:':443,'http:':80}[url.protocol] || 80,
        path: url.pathname+url.search,
        headers: headers,
      };

      let request = proto.request(opts,function(res) {
        const status = res.statusCode;
        var type = res.headers['content-type'];
        var charset = (type && type.match(/charset=(\S+)/)||[])[1];

        var that = {
          ok: (status/100|0)==2, // 200-299
          status: status,
          headers: Object.assign({},res.headers),
          pipe: (to) => res.pipe(to),
          body: () => new Promise((resolve,reject) => {
            var bufs = [];
            res.on('data', (chunk) => {
              bufs.push(chunk);
            });
            res.on('end', () => {
              let body = Buffer.concat(bufs);
              resolve(body);
            });
            res.on('error',(e) => {
              resolve({ error: {
                message: e.message,
                ...e
              }});
            })
          }),
          text: () => that.body().then(body => body.toString(charset||'utf-8')),
          json: () => that.body().then(b => JSON.parse(b)),
        };

        resolve(that);
      });

      request.on('error',(err) => {
        resolve({ error: {
          message: `${err.message}`,
          ...err
        }})
      });

      if(body) {
        if(typeof body.pipe == 'function') {
          body.pipe(request);
        } else {
          request.write(Buffer.from(options.body));
          request.end();
        }
      } else {
        request.end();
      }
    } catch (x) {
      console.log("Error thrown: ",x);
      reject(x);
    }
  });
}

module.exports = unet(execute);
module.exports.DEFAULT_MIDDLEWARE = unet.DEFAULT_MIDDLEWARE;
