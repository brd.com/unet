var test = require('tape');
var unet = require('../unet-browser');

test('test get', async function (t) {
  unet({ url: 'https://httpbin.org/get' }).then(res => {
    t.equal(res.result.headers.Host,'httpbin.org');
    t.end();
  }).catch(x => {
    t.notOk(x); t.end();
  });
});

test('test post',async function(t) {
  unet({ url: 'https://httpbin.org/post', method: 'post', body: { foo: 'bar' } }).then(response => {
    let result = response.result;
    
    t.equal(result.headers.Accept,'application/json');
    t.equal(result.headers['Content-Type'],'application/json');
    t.equal(result.json.foo,'bar');
    
    t.end();
  }).catch(x => {
    t.notOk(x);
    t.end();
  });
});

test('error',async function(t) {
  const r = await unet({ url: 'https://figgledyboo.notatld/', method: 'get' });
  t.equal(r.error,'0 - connection error');

  t.end();
});

