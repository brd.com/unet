const unet = require('./shared');

function execute(options,next) {
  return new Promise((resolve,reject) => {
    try {
      let request = new XMLHttpRequest();

      let method = options.method;
      let url = (options.baseURL||'') + options.url;

      if(!method) throw new Error("Must specify method");
      if(!url) throw new Error("must specify url");
      
      request.open(method , url , true);

      if('headers' in options) {
        for(var k in options.headers) {
          var v = options.headers[k];
          request.setRequestHeader(k,v);
        }
      }

      // default to sending with credentials:
      request.withCredentials = 'credentials' in options ? options.credentials=='include' : true;

      request.onload = () => resolve(response());
      request.onerror = (error) => resolve(response(error));

      request.onprogress = options.onProgress;

      if(options.onUploadProgress) {
        request.upload.onprogress = (event) => options.onUploadProgress((event.loaded / event.total)*100,event);
        request.upload.onload = (event) => options.onUploadProgress(100);
      }

      request.send(options.body || null);

      function response(error) {
			  let keys = [],
				    all = [],
				    headers = {},
				    header;

			  request.getAllResponseHeaders().replace(/^(.*?):[^\S\n]*([\s\S]*?)$/gm, (m, key, value) => {
				  keys.push(key = key.toLowerCase());
				  all.push([key, value]);
				  header = headers[key];
				  headers[key] = header ? `${header},${value}` : value;
			  });

        // TODO: no reason to make this work like fetch (this code was stolen
        // originally from unfetch), this should probably return an object
        // with a response, result (maybe responseText?), and error objects.
        // Probably 400 and 500-series responses get a response and an error,
        // and 200s get a result.  Everything always gets a response object,
        // which is probably similar to what fetch responds with.

        return {
				  ok: (request.status/100|0) == 2,		// 200-299
				  status: request.status,
				  statusText: request.status==0 ? 'connection error' : request.statusText,
				  url: request.responseURL,
				  clone: response,
				  text: () => Promise.resolve(request.responseText),
				  json: () => Promise.resolve(request.responseText).then(JSON.parse),
				  blob: () => Promise.resolve(new Blob([request.response])),
				  headers: headers,
			  };
      }
    } catch(x) {
      reject(x);
    }
  });
}

module.exports = unet(execute);
module.exports.DEFAULT_MIDDLEWARE = unet.DEFAULT_MIDDLEWARE;
