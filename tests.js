var test = require('tape');
var unet = require('./unet-node');
const { PassThrough } = require('stream');

const pause = (duration) => new Promise(res => setTimeout(res,duration));

test('test get', async function (t) {
  unet({ url: 'https://httpbin.org/get' }).then(res => {
    t.equal(res.result.headers.Host,'httpbin.org');
    t.end();
  }).catch(x => {
    t.notOk(x); t.end();
  });
});

test('test post',async function(t) {
  unet({ url: 'https://httpbin.org/post', method: 'post', body: { foo: 'bar' } }).then(response => {
    let result = response.result;
    
    t.equal(result.headers.Accept,'application/json');
    t.equal(result.headers['Content-Type'],'application/json');
    t.equal(result.json.foo,'bar');
    
    t.end();
  }).catch(x => {
    t.notOk(x);
    t.end();
  });
});

async function writeSlowly(stream,string) {
  for(var i = 0 ; i < string.length ; i++) {
    stream.write(string[i]);
    await pause(1);
  }
  stream.end();
}
  

test('test streaming post',async function(t) {
  const pass = new PassThrough();
  const p = unet({ url: 'https://httpbin.org/post', method: 'post', body: pass, headers: { 'content-type': 'text/plain' } });

  await writeSlowly(pass,'ok then',1);
  const { result } = await p;

  t.equal(result.data,'ok then',"Must come back with the same stuff");
  t.end();
});

